# Level Level Plugin list

## Introduction

Adds composer dependency version information to the wp-cli plugin list.

## Usage

In the folder that contains the theme `composer.json` use command
`wp llplugin list`. It has the same options as the regular `wp plugin list`
command.

## Example output

```
+------------------------------------------+----------+-----------+------------+
| 0-loader                                 | must-use | none      | 1.0.0      |
| composer/installers                      | composer | none      | v1.2.0     |
| hoppinger/advanced-custom-fields-wpcli   | composer | available | 1.2.1      |
| level-level/clarkson-core                | composer | none      | 0.1.6      |
| ll-premium/gravityforms                  | composer | none      | 2.1.1.9    |
| ll-premium/searchwp                      | composer | none      | 2.8.6      |
| ll-premium/searchwp-like                 | composer | none      | 2.4.4      |
| symfony/translation                      | composer | available | v2.7.7     |
| twig/extensions                          | composer | none      | v1.4.1     |
| twig/twig                                | composer | none      | v1.28.1    |
| wp-api/wp-api                            | composer | none      | 2.0-beta15 |
| wpackagist-plugin/advanced-custom-fields | composer | none      | 4.4.11     |
| wpackagist-plugin/cms-tree-page-view     | composer | none      | 1.3.4      |
| wpackagist-plugin/searchwp-api           | composer | none      | 1.1.0      |
| wpackagist-plugin/shortcode-ui           | composer | available | 0.6.2      |
| wpackagist-plugin/wp-api-menus           | composer | none      | 1.3.1      |
+------------------------------------------+----------+-----------+------------+
```