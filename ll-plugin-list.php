<?php
/**
 * Plugin Name: LL plugin list
 */
namespace LL;

if ( defined( 'WP_CLI' ) && WP_CLI ) {
  class PluginList extends \Plugin_Command{
    protected function get_all_items(){
      $items = parent::get_all_items();
      $composerList = shell_exec('composer show -l --no-ansi');
      $composerList = explode("\n", $composerList);
      foreach($composerList as $composerItem){
        if(!empty($composerItem)){
          $composerDetails = preg_split('/\s+/', $composerItem);
          $items[ $composerDetails[0] ] = array(
    				'name'           => $composerDetails[0],
    				'status'         => 'composer',
    				'update'         => ($composerDetails[1] != $composerDetails[2]),
    				'update_version' => $composerDetails[2],
    				'update_package' => NULL,
    				'version'        => $composerDetails[1],
    				'update_id'      => '',
    				'title'          => '',
    				'description'    => '',
    			);
        }
      }

      return $items;
    }
  }
  \WP_CLI::add_command('llplugin', 'LL\PluginList');
}
